### Under Construction
#### Latest Posts Plugin for Bludit

**The idea is to create a list of latest posts similar to that of the "Pages" and "Tags" plugins.**

The concept is a list of latest posts with the short description included.

The included plugin.php file is a working example of the concept.

Ideally, we would want to be able to limit the number of posts displayed.

This plugin will need to be completely re-written.

# Any help welcome!
