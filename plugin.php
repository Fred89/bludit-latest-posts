<?php

class pluginLatestPosts extends Plugin {

  public function init()
  {
    $this->dbFields = array(
      'label'=>'Latest Posts'
    );
  }

  public function form()
  {
    global $Language;

    $html  = '<div>';
    $html .= '<label>'.$Language->get('Plugin label').'</label>';
    $html .= '<input name="label" id="jslabel" type="text" value="'.$this->getDbField('label').'">';
    $html .= '</div>';

    return $html;
  }

  public function siteSidebar() // sitePosts from line added to kernel/boot/rules/80.plugins.php
  {
    global $Language;
    global $posts;
    global $Site, $Url;

    $html  = '<div class="plugin plugin-posts">';
    
		// Print the label if not empty
		$label = $this->getDbField('label');
		if( !empty($label) ) {
			$html .= '<h2>'.$label.'</h2>';
		}
    
    $html .= '<div class="plugin-content">';

    foreach($posts as $post)
    {
      // Print the posts
      $html .= '<ul>';
      $html .= '<li>';
      $html .= '<a class="p-title '.( ($post->key()==$Url->slug())?' active':'').'" href="'.$post->permalink().'">'.$post->title().'</a>';
      $html .= '</li>';
      $html .= '<li class="p-description">'.$post->description().'</li>';
      $html .= '</ul>';
    }
    $html .= '</div>';
    $html .= '</div>';

    return $html;
  }
}
